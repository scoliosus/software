PROJECT_NAME     := ScoliosUs
TARGETS          := nrf52840_xxaa
OUTPUT_DIRECTORY := _build

SDK_ROOT := sdk
PROJ_DIR := src

GNU_INSTALL_ROOT :=
GNU_PREFIX := arm-none-eabi

$(OUTPUT_DIRECTORY)/nrf52840_xxaa.out: \
  LINKER_SCRIPT  := linker.ld

# Source files common to all targets
##################################################
# SDK 
##################################################
SRC_FILES = $(SDK_ROOT)/modules/nrfx/mdk/gcc_startup_nrf52840.S
SRC_FILES += $(SDK_ROOT)/components/boards/boards.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/croutine.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/event_groups.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/portable/MemMang/heap_1.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/list.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/portable/GCC/nrf52/port.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/portable/CMSIS/nrf52/port_cmsis.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/portable/CMSIS/nrf52/port_cmsis_systick.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/queue.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/stream_buffer.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/tasks.c 
SRC_FILES += $(SDK_ROOT)/external/freertos/source/timers.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/button/app_button.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/util/app_error.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/util/app_error_handler_gcc.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/util/app_error_weak.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/timer/app_timer_freertos.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/util/app_util_platform.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/util/nrf_assert.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/atomic/nrf_atomic.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/balloc/nrf_balloc.c 
SRC_FILES += $(SDK_ROOT)/external/fprintf/nrf_fprintf.c 
SRC_FILES += $(SDK_ROOT)/external/fprintf/nrf_fprintf_format.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/memobj/nrf_memobj.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/ringbuf/nrf_ringbuf.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/strerror/nrf_strerror.c 
SRC_FILES += $(SDK_ROOT)/integration/nrfx/legacy/nrf_drv_clock.c 
SRC_FILES += $(SDK_ROOT)/components/drivers_nrf/nrf_soc_nosd/nrf_nvic.c 
SRC_FILES += $(SDK_ROOT)/components/drivers_nrf/nrf_soc_nosd/nrf_soc.c 
SRC_FILES += $(SDK_ROOT)/modules/nrfx/drivers/src/nrfx_clock.c 
SRC_FILES += $(SDK_ROOT)/modules/nrfx/drivers/src/nrfx_gpiote.c 
SRC_FILES += $(SDK_ROOT)/modules/nrfx/drivers/src/nrfx_power_clock.c 
SRC_FILES += $(SDK_ROOT)/modules/nrfx/drivers/src/nrfx_power.c
SRC_FILES += $(SDK_ROOT)/components/libraries/bsp/bsp.c 
SRC_FILES += $(SDK_ROOT)/modules/nrfx/mdk/system_nrf52840.c
SRC_FILES += $(SDK_ROOT)/integration/nrfx/legacy/nrf_drv_spi.c
SRC_FILES += $(SDK_ROOT)/modules/nrfx/drivers/src/nrfx_spim.c
SRC_FILES += $(SDK_ROOT)/modules/nrfx/drivers/src/nrfx_spi.c
# SRC_FILES += $(SDK_ROOT)/external/segger_rtt/SEGGER_RTT_Syscalls_GCC.c
SRC_FILES += $(SDK_ROOT)/external/segger_rtt/SEGGER_RTT.c
SRC_FILES += $(SDK_ROOT)/external/segger_rtt/SEGGER_RTT_printf.c
SRC_FILES += $(SDK_ROOT)/components/libraries/queue/nrf_queue.c
SRC_FILES += $(SDK_ROOT)/components/libraries/usbd/app_usbd.c
SRC_FILES += $(SDK_ROOT)/components/libraries/usbd/class/cdc/acm/app_usbd_cdc_acm.c
SRC_FILES += $(SDK_ROOT)/components/libraries/usbd/app_usbd_core.c
SRC_FILES += $(SDK_ROOT)/components/libraries/usbd/app_usbd_string_desc.c
SRC_FILES += $(SDK_ROOT)/components/drivers_nrf/usbd/nrf_drv_usbd.c
SRC_FILES += $(SDK_ROOT)/components/libraries/pwr_mgmt/nrf_pwr_mgmt.c
SRC_FILES += $(SDK_ROOT)/integration/nrfx/legacy/nrf_drv_power.c
SRC_FILES += $(SDK_ROOT)/components/libraries/fds/fds.c 
SRC_FILES += $(SDK_ROOT)/components/libraries/fstorage/nrf_fstorage_nvmc.c
SRC_FILES += $(SDK_ROOT)/components/libraries/fstorage/nrf_fstorage.c
SRC_FILES += $(SDK_ROOT)/components/libraries/queue/nrf_queue.c
SRC_FILES += $(SDK_ROOT)/components/libraries/atomic_fifo/nrf_atfifo.c
SRC_FILES += $(SDK_ROOT)/components/libraries/experimental_section_vars/nrf_section_iter.c
SRC_FILES += $(SDK_ROOT)/modules/nrfx/hal/nrf_nvmc.c



SDK_INCLUDE_FOLDERS =  $(SDK_ROOT)/components 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/toolchain/cmsis/include
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/util
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/ringbuf 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/drivers_nrf/nrf_soc_nosd
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/log/src
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/log
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/timer 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/strerror 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/atomic 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/atomic_fifo
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/balloc 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/boards 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/bsp 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/fds
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/button 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/experimental_section_vars 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/memobj 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/freertos/source/include 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/freertos/portable/CMSIS/nrf52 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/freertos/portable/GCC/nrf52 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/fprintf 
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/segger_rtt
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/integration/nrfx
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/integration/nrfx/legacy
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/drivers
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/drivers/include
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/drivers/src/prs
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/hal
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/soc
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/mdk
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/templates
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/modules/nrfx/templates/nRF52840
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/segger_rtt
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/cli
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/cli/cdc_acm
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/queue/
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/usbd
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/usbd/class/cdc
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/usbd/class/cdc/acm
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/drivers_nrf/usbd
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/delay
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/pwr_mgmt
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/fstorage
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/utf_converter
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/mutex
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/external/fnmatch
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/fds
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/atomic_fifo
SDK_INCLUDE_FOLDERS += $(SDK_ROOT)/components/libraries/fstorage



##################################################
# Project
##################################################
SRC_FILES += $(PROJ_DIR)/main.c
SRC_FILES += $(PROJ_DIR)/device_drivers/lsm9ds1/lsm9ds1_driver.c
SRC_FILES += $(PROJ_DIR)/usb_transport/usb_transport.c
SRC_FILES += $(PROJ_DIR)/usb_transport/usb_transport_format.c
SRC_FILES += $(PROJ_DIR)/timer/timer.c
SRC_FILES += $(PROJ_DIR)/tasks/sampling_task.c

PROJECT_INCLUDE_FOLDERS =  config
PROJECT_INCLUDE_FOLDERS += $(PROJ_DIR)/device_drivers/lsm9ds1/inc
PROJECT_INCLUDE_FOLDERS += $(PROJ_DIR)/usb_transport/inc
PROJECT_INCLUDE_FOLDERS += $(PROJ_DIR)/timer/inc
PROJECT_INCLUDE_FOLDERS += $(PROJ_DIR)/tasks/inc



INC_FOLDERS += $(PROJECT_INCLUDE_FOLDERS)
INC_FOLDERS += $(SDK_INCLUDE_FOLDERS)

# Libraries common to all targets
LIB_FILES += \

# Optimization flags
OPT = -O3 -g3
# Uncomment the line below to enable link time optimization
#OPT += -flto

# C flags common to all targets
CFLAGS += $(OPT)
CFLAGS += -DBOARD_PCA10056
CFLAGS += -DCONFIG_GPIO_AS_PINRESET
CFLAGS += -DFLOAT_ABI_HARD
CFLAGS += -DFREERTOS
CFLAGS += -DNRF52840_XXAA
CFLAGS += -mcpu=cortex-m4
CFLAGS += -mthumb -mabi=aapcs
CFLAGS += -Wall -Werror
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
# keep every function in a separate section, this allows linker to discard unused ones
CFLAGS += -ffunction-sections -fdata-sections -fno-strict-aliasing
CFLAGS += -fno-builtin -fshort-enums

# C++ flags common to all targets
CXXFLAGS += $(OPT)

# Assembler flags common to all targets
ASMFLAGS += -g3
ASMFLAGS += -mcpu=cortex-m4
ASMFLAGS += -mthumb -mabi=aapcs
ASMFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
ASMFLAGS += -DBOARD_PCA10056
ASMFLAGS += -DCONFIG_GPIO_AS_PINRESET
ASMFLAGS += -DFLOAT_ABI_HARD
ASMFLAGS += -DFREERTOS
ASMFLAGS += -DNRF52840_XXAA

# Linker flags
LDFLAGS += $(OPT)
LDFLAGS += -mthumb -mabi=aapcs -L$(SDK_ROOT)/modules/nrfx/mdk -T$(LINKER_SCRIPT)
LDFLAGS += -mcpu=cortex-m4
LDFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
# let linker dump unused sections
LDFLAGS += -Wl,--gc-sections
# use newlib in nano version
LDFLAGS += --specs=nano.specs

nrf52840_xxaa: CFLAGS += -D__HEAP_SIZE=8192
nrf52840_xxaa: CFLAGS += -D__STACK_SIZE=8192
nrf52840_xxaa: ASMFLAGS += -D__HEAP_SIZE=8192
nrf52840_xxaa: ASMFLAGS += -D__STACK_SIZE=8192

# Add standard libraries at the very end of the linker input, after all objects
# that may need symbols provided by these libraries.
LIB_FILES += -lc -lnosys -lm


.PHONY: default help

# Default target - first one defined
default: nrf52840_xxaa

# Print all targets that can be built
help:
	@echo following targets are available:
	@echo		nrf52840_xxaa
	@echo		flash      - flashing binary

TEMPLATE_PATH := $(SDK_ROOT)/components/toolchain/gcc


include $(TEMPLATE_PATH)/Makefile.common

$(foreach target, $(TARGETS), $(call define_target, $(target)))

.PHONY: flash erase

# Flash the program
flash: default
	@echo Flashing: $(OUTPUT_DIRECTORY)/nrf52840_xxaa.hex
	nrfjprog -f nrf52 --program $(OUTPUT_DIRECTORY)/nrf52840_xxaa.hex --sectorerase
	nrfjprog -f nrf52 --reset

erase:
	nrfjprog -f nrf52 --eraseall

SDK_CONFIG_FILE := ../config/sdk_config.h
CMSIS_CONFIG_TOOL := $(SDK_ROOT)/external_tools/cmsisconfig/CMSIS_Configuration_Wizard.jar
sdk_config:
	java -jar $(CMSIS_CONFIG_TOOL) $(SDK_CONFIG_FILE)



