/**CHeaderFile*****************************************************************
  FileName    [ timer.h ]
  PackageName [ timer ]
  Synopsis    [ required ]
  Description [ optional ]
  SeeAlso     [ optional ]
  Author      [ Dalin Riches ]
******************************************************************************/

#ifndef TIMER_H
#define TIMER_H

/*---------------------------------------------------------------------------*/
/* Includes                                                                  */
/*---------------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include "nrf52840.h"
#include "nrf52840_bitfields.h"
/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/
#define TIMER_MODE            (TIMER_MODE_MODE_Timer)
#define TIMER_BITMODE         (TIMER_BITMODE_BITMODE_16Bit)
#define TIMER_PRESCALER       (0x8 & TIMER_PRESCALER_PRESCALER_Msk)
#define TIMER_CC              (0)
#define TIMER_CC_INTEN        (1 << (TIMER_CC + TIMER_INTENSET_COMPARE0_Pos))
#define TIMER_SHORTS          (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos)
#define TIMER_ONESHOT_SHORTS  (TIMER_SHORTS_COMPARE0_STOP_Enabled << TIMER_SHORTS_COMPARE0_STOP_Pos)
#define TIMER_TICK_FREQ       (16000000/(1 << TIMER_PRESCALER))

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Stucture declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
void init_timer(NRF_TIMER_Type* timer);
void init_timer_oneshot(NRF_TIMER_Type* timer);

void timer_start(NRF_TIMER_Type* timer);
void timer_stop(NRF_TIMER_Type* timer);
void timer_reset(NRF_TIMER_Type* timer);

void timer_set_trigger_freq(NRF_TIMER_Type* timer, uint32_t frequency);

#endif // !TIMER_H