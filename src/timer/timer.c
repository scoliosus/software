#include "timer.h"

void init_timer(NRF_TIMER_Type* timer)
{
	timer->TASKS_STOP = true;
	timer->TASKS_CLEAR = true;

	timer->MODE 		= TIMER_MODE;
	timer->BITMODE 		= TIMER_BITMODE;
	timer->PRESCALER 	= TIMER_PRESCALER;
	timer->SHORTS		= TIMER_SHORTS;
	timer->CC[TIMER_CC] = TIMER_TICK_FREQ;
	timer->INTENCLR		= TIMER_CC_INTEN;
}

void init_timer_oneshot(NRF_TIMER_Type* timer)
{
	timer->TASKS_STOP = true;
	timer->TASKS_CLEAR = true;

	timer->MODE 		= TIMER_MODE;
	timer->BITMODE 		= TIMER_BITMODE;
	timer->PRESCALER 	= TIMER_PRESCALER;
	timer->SHORTS		= TIMER_SHORTS;
	timer->CC[TIMER_CC] = TIMER_TICK_FREQ;
	timer->INTENCLR		= TIMER_CC_INTEN;
}

void timer_start(NRF_TIMER_Type* timer)
{
	timer->TASKS_START = true;
}

void timer_stop(NRF_TIMER_Type* timer)
{
	timer->TASKS_STOP = true;
}

void timer_reset(NRF_TIMER_Type* timer)
{
	timer->TASKS_CLEAR = true;
}

void timer_set_trigger_freq(NRF_TIMER_Type* timer, uint32_t frequency_hz)
{
	if (frequency_hz == 0)
	{
		timer->INTENCLR	= TIMER_CC_INTEN;
		return;
	}
	else if (frequency_hz >= TIMER_TICK_FREQ)
	{
		timer->CC[TIMER_CC] = TIMER_TICK_FREQ;
		timer->INTENSET		= TIMER_CC_INTEN;
		return;
	}

	timer->CC[TIMER_CC] = TIMER_TICK_FREQ/frequency_hz;
	timer->INTENSET		= TIMER_CC_INTEN;
}