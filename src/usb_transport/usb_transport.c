/**CFile***********************************************************************
  FileName    [ usbd_cli.c ]
  PackageName [ usbd_cli ]
  Synopsis    [ required ]
  Description [ optional ]
  SeeAlso     [ optional ]
  Author      [ Dalin Riches ]
******************************************************************************/
#include "usb_transport.h"

#include "semphr.h"
#include "queue.h"
#include "nrf_drv_usbd.h"
#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_error.h"

/*---------------------------------------------------------------------------*/
/* Stucture declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
static void transport_ev_handler(app_usbd_class_inst_t const * p_inst,
                                 app_usbd_cdc_acm_user_event_t   event);
static void usbd_user_ev_handler(app_usbd_event_type_t event);


void transport_process(void);     
void usb_init(void);


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
TaskHandle_t  usb_transport_task_handle;      /**< Reference to CLI task */
SemaphoreHandle_t txSemaphore;
QueueHandle_t packet_queue_handle;

usb_packet_t tx_packet_buffer;

volatile bool usbd_port_open = false;
volatile bool usbd_transmit_in_progress = false;

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/
#define CLI_COMM_INTERFACE  (0)
#define CLI_COMM_EPIN       (NRF_DRV_USBD_EPIN2)

#define CLI_DATA_INTERFACE  (1)
#define CLI_DATA_EPIN       (NRF_DRV_USBD_EPIN1)
#define CLI_COMM_EPOUT      (NRF_DRV_USBD_EPOUT1)

APP_USBD_CDC_ACM_GLOBAL_DEF (transport_cdc_acm,
		  	transport_ev_handler,
		  	CLI_COMM_INTERFACE,
		  	CLI_DATA_INTERFACE,
		  	CLI_COMM_EPIN,
		  	CLI_DATA_EPIN,
		  	CLI_COMM_EPOUT,
		  	APP_USBD_CDC_COMM_PROTOCOL_AT_V250 
	);	 

/*---------------------------------------------------------------------------*/
/* Definition of external functions                                          */
/*---------------------------------------------------------------------------*/
void init_usb_transport()
{
    packet_queue_handle = xQueueCreate(USB_PACKET_QUEUE_SIZE, sizeof(usb_packet_t));

    usb_init();
}

void usb_transport_task_function(void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);                       

    while (true)
    {
        transport_process();
        portYIELD();
    }
}

/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/
void usb_init(void)
{
    ret_code_t ret;

    static const app_usbd_config_t usbd_config = {
        .ev_handler = app_usbd_event_execute,
        .ev_state_proc = usbd_user_ev_handler
    };

    ret = app_usbd_init(&usbd_config);
    APP_ERROR_CHECK(ret);

    app_usbd_class_inst_t const * transport_usbd_cdc_acm_instance =
        app_usbd_cdc_acm_class_inst_get(&transport_cdc_acm);

    ret = app_usbd_class_append(transport_usbd_cdc_acm_instance);
    APP_ERROR_CHECK(ret);

    ret = app_usbd_power_events_enable();
    APP_ERROR_CHECK(ret);

}

void transport_process(void) {
    // Only capable of transmitting packets at the moment
    if (xQueueReceive(packet_queue_handle, (void *) &tx_packet_buffer, 0))
    {
        app_usbd_cdc_acm_write(&transport_cdc_acm, (void *) &tx_packet_buffer, 3 + tx_packet_buffer.payload_size);
    }
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/
static void usbd_user_ev_handler(app_usbd_event_type_t event)
{
    switch (event)
    {
        case APP_USBD_EVT_STOPPED: 
            app_usbd_disable();
            break;
        case APP_USBD_EVT_POWER_DETECTED:
            if (!nrf_drv_usbd_is_enabled())
            {
                app_usbd_enable();
            }
            break;
        case APP_USBD_EVT_POWER_REMOVED:
            app_usbd_stop();
            break;
        case APP_USBD_EVT_POWER_READY:
            app_usbd_start();
            break;
        default:
            break;
    }
}

static void transport_ev_handler(app_usbd_class_inst_t const * p_inst,
                                 app_usbd_cdc_acm_user_event_t   event)
{
    switch (event)
    {
        case APP_USBD_CDC_ACM_USER_EVT_RX_DONE:
            break;
        case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
            {
                usbd_transmit_in_progress = false;
            }
            break;
        case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN:
            usbd_port_open = true;
            break;
        case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
            usbd_port_open = false;
            break;
        default:
            break;
    }
}

void usb_send_packet(usb_packet_t *packet)
{
    xQueueSend(packet_queue_handle, (void *) (packet), 0);
}