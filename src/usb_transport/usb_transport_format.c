#include "usb_transport.h"

#include <stdint.h>
#include <stdbool.h>

uint64_t get_mac_addr(void) {
    uint64_t mac_addr = 0;

    mac_addr |= NRF_FICR->DEVICEADDR[0];
    mac_addr |= ((uint64_t)(NRF_FICR->DEVICEADDR[1] & 0xFFFF) << 32);
	mac_addr |= 0xC00000000000;

    return mac_addr;
}

void usb_format_nodes_data(usb_packet_t *packet, lsm9ds1_output_data_t *nodes, uint8_t number_of_devices, uint8_t type)
{
	uint8_t payload_size = 0;
	uint8_t payload_offset = 0;

	packet->start_token = USB_PACKET_START_TOKEN;
	packet->type = type;

	for (uint8_t device = 0; device < number_of_devices; device++)
	{
		payload_offset = device * 12;
		payload_size = payload_offset + 12;

		packet->payload[payload_offset +  0] = nodes[device].accel_x.bytes.high;  
		packet->payload[payload_offset +  1] = nodes[device].accel_x.bytes.low;
		packet->payload[payload_offset +  2] = nodes[device].accel_y.bytes.high;
		packet->payload[payload_offset +  3] = nodes[device].accel_y.bytes.low;
		packet->payload[payload_offset +  4] = nodes[device].accel_z.bytes.high;
		packet->payload[payload_offset +  5] = nodes[device].accel_z.bytes.low;
		packet->payload[payload_offset +  6] = nodes[device].gyro_x.bytes.high;
		packet->payload[payload_offset +  7] = nodes[device].gyro_x.bytes.low;
		packet->payload[payload_offset +  8] = nodes[device].gyro_y.bytes.high;
		packet->payload[payload_offset +  9] = nodes[device].gyro_y.bytes.low;
		packet->payload[payload_offset + 10] = nodes[device].gyro_z.bytes.high;
		packet->payload[payload_offset + 11] = nodes[device].gyro_z.bytes.low;
	}

	packet->payload_size = payload_size;
}

void usb_packet_send_sample(lsm9ds1_output_data_t *nodes, uint8_t number_of_devices)
{
	usb_packet_t packet;

	usb_format_nodes_data(&packet, nodes, number_of_devices, 0x1);

	usb_send_packet(&packet);
}

void usb_packet_send_target(lsm9ds1_output_data_t *nodes, uint8_t number_of_devices)
{
	usb_packet_t packet;

	usb_format_nodes_data(&packet, nodes, number_of_devices, 0x4);

	usb_send_packet(&packet);
}

void usb_packet_send_start()
{
	usb_packet_t packet;
	uint64_t mac_addr = get_mac_addr();

	packet.start_token = USB_PACKET_START_TOKEN;
	packet.type = 0x2;
	packet.payload_size = 6;

	for (uint8_t i = 0; i < 6; i++)
	{
		packet.payload[i] = ((mac_addr >> (i*8)) & 0xFF);
	}

	usb_send_packet(&packet);
}

void usb_packet_send_stop()
{
	usb_packet_t packet;

	packet.start_token = USB_PACKET_START_TOKEN;
	packet.type = 0x3;
	packet.payload_size = 0;

	usb_send_packet(&packet);
}