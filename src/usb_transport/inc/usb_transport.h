/**CHeaderFile*****************************************************************
  FileName    [ usb_transport.h ]
  PackageName [ usb_transport ]
  Synopsis    [ required ]
  Description [ optional ]
  SeeAlso     [ optional ]
  Author      [ Dalin Riches ]
******************************************************************************/
#ifndef USBD_TRANSPORT_H
#define USBD_TRANSPORT_H

/*---------------------------------------------------------------------------*/
/* Includes                                                                  */
/*---------------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "app_usbd.h"
#include "lsm9ds1_portmap.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/
#define USB_PACKET_MAX_PAYLOAD_SIZE   (256)
#define USB_PACKET_QUEUE_SIZE         (10) // must be greater than 1


#define USB_PACKET_START_TOKEN        ('$')

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef struct usb_packet usb_packet_t;

struct usb_packet {
  uint8_t start_token;
  uint8_t type;
  uint8_t payload_size;
  uint8_t payload[USB_PACKET_MAX_PAYLOAD_SIZE];
};

/*---------------------------------------------------------------------------*/
/* Stucture declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
extern const app_usbd_config_t usbd_config;
extern QueueHandle_t packet_queue_handle;
extern TaskHandle_t usb_transport_task_handle;

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
void init_usb_transport();
void usb_transport_task_function(void * pvParameter);

void usb_send_packet(usb_packet_t *packet);

void usb_packet_send_start();
void usb_packet_send_stop();
void usb_packet_send_sample(lsm9ds1_output_data_t *nodes, uint8_t number_of_devices);
void usb_packet_send_target(lsm9ds1_output_data_t *nodes, uint8_t number_of_devices);


#endif // !USBD_TRANSPORT_H
