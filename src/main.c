#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

#include "nrf_drv_clock.h"
#include "nrf_gpio.h"
#include "app_error.h"

#include "lsm9ds1_driver.h"
#include "sampling_task.h"
#include "usb_transport.h"

void init_gpio(void) {
    NRF_P0->OUT = NRF_P0->OUT | GPIO_OUT_PIN28_Msk
                              | GPIO_OUT_PIN27_Msk
                              | GPIO_OUT_PIN26_Msk
                              | GPIO_OUT_PIN3_Msk
                              | GPIO_OUT_PIN4_Msk;

    NRF_P0->DIR = NRF_P0->DIR | GPIO_DIR_PIN28_Msk
                              | GPIO_DIR_PIN26_Msk
                              | GPIO_OUT_PIN3_Msk
                              | GPIO_OUT_PIN4_Msk;

    NRF_P1->DIR = NRF_P1->DIR | GPIO_DIR_PIN1_Msk
                              | GPIO_DIR_PIN2_Msk
                              | GPIO_DIR_PIN3_Msk
                              | GPIO_DIR_PIN4_Msk
                              | GPIO_DIR_PIN10_Msk
                              | GPIO_DIR_PIN11_Msk;

    NRF_P1->OUT = NRF_P1->OUT | GPIO_OUT_PIN1_Msk
                              | GPIO_OUT_PIN2_Msk
                              | GPIO_OUT_PIN3_Msk
                              | GPIO_OUT_PIN4_Msk
                              | GPIO_DIR_PIN10_Msk
                              | GPIO_DIR_PIN11_Msk;

    nrf_gpio_cfg_sense_input(NRF_GPIO_PIN_MAP(0,25), NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW); // Button 4
    nrf_gpio_cfg_sense_input(NRF_GPIO_PIN_MAP(0,24), NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW); // Button 3
    nrf_gpio_cfg_output(NRF_GPIO_PIN_MAP(0,13));
    nrf_gpio_cfg_output(NRF_GPIO_PIN_MAP(0,14));
    nrf_gpio_cfg_output(NRF_GPIO_PIN_MAP(0,15));
    nrf_gpio_cfg_output(NRF_GPIO_PIN_MAP(0,16));
    nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,13));
    nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,14));
    nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,15));
    nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,16));
}

void TIMER2_IRQHandler(void) 
{
    BaseType_t xHigherPriorityTaskWoken;

    if (NRF_TIMER2->EVENTS_COMPARE[0] != 0)
    {
        NRF_TIMER2->EVENTS_COMPARE[0] = 0;

        xHigherPriorityTaskWoken = pdFALSE;
        xEventGroupSetBitsFromISR(sampling_events_in_handle, SAMPLING_INEVENT_SAMPLE_TICK_MSK, &xHigherPriorityTaskWoken);

        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

/**
 * Accelerometer ISR
 */
void GPIOTE_IRQHandler(void)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    EventBits_t events = 0;

    // Check to see if an event occurred
    if (NRF_GPIOTE->EVENTS_PORT == 1)
    {
        // clear event
        NRF_GPIOTE->EVENTS_PORT = 0;

        // Check to see if the right gpio pin triggered the interrupt
        if (((NRF_P0->PIN_CNF[25] & GPIO_PIN_CNF_SENSE_Msk) == (GPIO_PIN_CNF_SENSE_Low << GPIO_PIN_CNF_SENSE_Pos))
            && !(NRF_P0->IN & (1 << 25)))
        {
            events |= SAMPLING_INEVENT_TARGET_MODE_MSK;
        }

        if (((NRF_P0->PIN_CNF[24] & GPIO_PIN_CNF_SENSE_Msk) == (GPIO_PIN_CNF_SENSE_Low << GPIO_PIN_CNF_SENSE_Pos))
            && !(NRF_P0->IN & (1 << 24)))
        {
            events |= SAMPLING_INEVENT_ENABLE_MSK;
        }

        xEventGroupSetBitsFromISR(sampling_events_in_handle, events, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}


int main(void)
{
    ret_code_t err_code;
    /* Initialize clock driver for better time accuracy in FREERTOS */
    err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    init_gpio();
    init_accels();
    init_usb_transport();

    sampling_events_in_handle = xEventGroupCreate();

    NRF_GPIOTE->INTENSET = GPIOTE_INTENSET_PORT_Msk;
    NVIC_EnableIRQ(GPIOTE_IRQn);
    NVIC_EnableIRQ(TIMER2_IRQn);

    UNUSED_VARIABLE(xTaskCreate(sampling_task_function, "DATA", configMINIMAL_STACK_SIZE + 200, NULL, 2, &sampling_task_handle));
    UNUSED_VARIABLE(xTaskCreate(usb_transport_task_function, "USB", configMINIMAL_STACK_SIZE + 200, NULL, 0, &usb_transport_task_handle));
    /* Activate deep sleep mode */
     SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

    /* Start FreeRTOS scheduler. */
    vTaskStartScheduler();

    while (true)
    {
        /* FreeRTOS should not be here... FreeRTOS goes back to the start of stack
         * in vTaskStartScheduler function. */
    }
}
