#ifndef LSM9DS1_DRIVER_H
#define LSM9DS1_DRIVER_H

#include "lsm9ds1_portmap.h"


extern lsm9ds1_device_t nodes[8];

void init_accels(void);
bool lsm9ds1_sample(lsm9ds1_device_t *device, lsm9ds1_output_data_t *data);
void lsm9ds1_avg_samples(lsm9ds1_output_data_t *sample1, lsm9ds1_output_data_t *sample2);

#endif // !LSM9DS1_DRIVER_H`