#ifndef LSM9DS1_PORTMAP_H
#define LSM9DS1_PORTMAP_H

//Includes
#include <stdint.h>
#include "nrf52840.h"
#include "nrf_drv_spi.h"

// Address Definitions
#define LSM9DS1_ACT_THS_ADDR                        (0x04)
#define LSM9DS1_ACT_DUR_ADDR                        (0x05)
#define LSM9DS1_INT_GEN_CFG_XL_ADDR                 (0x06)
#define LSM9DS1_INT_GEN_THS_X_XL_ADDR               (0x07)
#define LSM9DS1_INT_GEN_THS_Y_XL_ADDR               (0x08)
#define LSM9DS1_INT_GEN_THS_Z_XL_ADDR               (0x09)
#define LSM9DS1_INT_GEN_DUR_XL_ADDR                 (0x0A)
#define LSM9DS1_REFERENCE_G_ADDR                    (0x0B)
#define LSM9DS1_INT1_CTRL_ADDR                      (0x0C)
#define LSM9DS1_INT2_CTRL_ADDR                      (0x0D)

#define LSM9DS1_WHO_AM_I_ADDR                       (0x0F)
#define LSM9DS1_CTRL_REG1_G_ADDR                    (0x10)
#define LSM9DS1_CTRL_REG2_G_ADDR                    (0x11)
#define LSM9DS1_CTRL_REG3_G_ADDR                    (0x12)
#define LSM9DS1_ORIENT_CFG_G_ADDR                   (0x13)
#define LSM9DS1_INT_GEN_SRC_G_ADDR                  (0x14)
#define LSM9DS1_OUT_TEMP_L_ADDR                     (0x15)
#define LSM9DS1_OUT_TEMP_H_ADDR                     (0x16)
#define LSM9DS1_STATUS_REG_ADDR                     (0x17)
#define LSM9DS1_OUT_X_L_G_ADDR                      (0x18)
#define LSM9DS1_OUT_X_H_G_ADDR                      (0x19)
#define LSM9DS1_OUT_Y_L_G_ADDR                      (0x1A)
#define LSM9DS1_OUT_Y_H_G_ADDR                      (0x1B)
#define LSM9DS1_OUT_Z_L_G_ADDR                      (0x1C)
#define LSM9DS1_OUT_Z_H_G_ADDR                      (0x1D)
#define LSM9DS1_CTRL_REG4_ADDR                      (0x1E)
#define LSM9DS1_CTRL_REG5_XL_ADDR                   (0x1F)
#define LSM9DS1_CTRL_REG6_XL_ADDR                   (0x20)
#define LSM9DS1_CTRL_REG7_XL_ADDR                   (0x21)
#define LSM9DS1_CTRL_REG8_ADDR                      (0x22)
#define LSM9DS1_CTRL_REG9_ADDR                      (0x23)
#define LSM9DS1_CTRL_REG10_ADDR                     (0x24)
#define LSM9DS1_INT_GEN_SRC_XL_ADDR                 (0x26)

#define LSM9DS1_OUT_X_L_XL_ADDR                     (0x28)
#define LSM9DS1_OUT_X_H_XL_ADDR                     (0x29)
#define LSM9DS1_OUT_Y_L_XL_ADDR                     (0x2A)
#define LSM9DS1_OUT_Y_H_XL_ADDR                     (0x2B)
#define LSM9DS1_OUT_Z_L_XL_ADDR                     (0x2C)
#define LSM9DS1_OUT_Z_H_XL_ADDR                     (0x2D)
#define LSM9DS1_FIFO_CTRL_ADDR                      (0X2E)
#define LSM9DS1_FIFO_SRC_ADDR                       (0x2F)
#define LSM9DS1_INT_GEN_CFG_G_ADDR                  (0x30)
#define LSM9DS1_INT_GEN_THS_XH_G_ADDR               (0x31)
#define LSM9DS1_INT_GEN_THS_XL_G_ADDR               (0x32)
#define LSM9DS1_INT_GEN_THS_YH_G_ADDR               (0x33)
#define LSM9DS1_INT_GEN_THS_YL_G_ADDR               (0x34)
#define LSM9DS1_INT_GEN_THS_ZH_G_ADDR               (0x35)
#define LSM9DS1_INT_GEN_THS_ZL_G_ADDR               (0x36)
#define LSM9DS1_INT_GEN_DUR_G_ADDR                  (0x37)

#define LSM9DS1_READ_ADDRESS(addr)                  (addr | 0x80)
#define LSM9DS1_WRITE_ADDRESS(addr)                 (addr | 0x00)

#define LSM9DS1_WHO_AM_I_EXPECTED_VALUE             (0x68)


// Register Bitfield/union defintion
union LSM9DS1_OUTPUT
{
    struct 
    {
        uint8_t low;
        uint8_t high;
    } bytes;

    int16_t value;
};

union LSM9DS1_ACT_THS 
{
    struct 
    {
        uint8_t act_ths                             :7;
        uint8_t sleep_on_inact_en                   :1; 
    } fields;

    uint8_t value;
};

union LSM9DS1_INT_GEN_CFG_XL
{
    struct 
    {
        uint8_t xlie_xl                             :1;
        uint8_t xhie_xl                             :1;
        uint8_t ylie_xl                             :1;
        uint8_t yhie_xl                             :1;
        uint8_t zlie_xl                             :1;
        uint8_t zhie_xl                             :1;
        uint8_t sixd                                :1;
        uint8_t aoi_xl                              :1;
    } fields;

    uint8_t value;
};

union LSM9DS1_INT_GEN_DUR_XL 
{
    struct 
    {
        uint8_t dur_xl                              :6;
        uint8_t wait_xl                             :1;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_INT1_CTRL lsm9ds1_int1_ctrl_t;
union LSM9DS1_INT1_CTRL
{
    struct 
    {
        uint8_t int1_drdy_xl                        :1;
        uint8_t int1_drdy_g                         :1;
        uint8_t int1_boot                           :1;
        uint8_t int1_fth                            :1;
        uint8_t int1_ovr                            :1;
        uint8_t int1_fss5                           :1;
        uint8_t int1_ig_xl                          :1;
        uint8_t int1_ig_g                           :1;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_INT2_CTRL lsm9ds1_int2_ctrl_t;
union LSM9DS1_INT2_CTRL 
{
    struct 
    {
        uint8_t int2_drdy_xl                        :1;
        uint8_t int2_drdy_g                         :1;
        uint8_t int2_drdy_temp                      :1;
        uint8_t int2_fth                            :1;
        uint8_t int2_ovr                            :1;
        uint8_t int2_fss5                           :1;
        uint8_t                                     :1;
        uint8_t int2_inact                          :1;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG1_G lsm9ds1_ctrl_reg1_g_t;
union LSM9DS1_CTRL_REG1_G
{
    struct 
    {
        uint8_t bw_g                                :2;
        uint8_t                                     :1;
        uint8_t fs_g                                :2;
        uint8_t odr_g                               :3;
    } fields;

    uint8_t value;
};
 
typedef union LSM9DS1_CTRL_REG2_G lsm9ds1_ctrl_reg2_g_t;
union LSM9DS1_CTRL_REG2_G 
{
    struct 
    {
        uint8_t out_sel                             :2;
        uint8_t int_sel                             :2;
        uint8_t                                     :4;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG3_G lsm9ds1_ctrl_reg3_g_t;
union LSM9DS1_CTRL_REG3_G 
{
    struct 
    {
        uint8_t hpcf_g                              :4;
        uint8_t                                     :2;
        uint8_t hp_en                               :1;
        uint8_t lp_mode                             :1;
    } fields;
    
    uint8_t value;
};

union LSM9DS1_ORIENT_CFG_G {
    struct 
    {
        uint8_t orient                              :3;
        uint8_t signz_g                             :1;
        uint8_t signy_g                             :1;
        uint8_t signx_g                             :1;
        uint8_t                                     :2;
    } fields;

    uint8_t value;
};

  
union LSM9DS1_INT_GEN_SRC_G
{
    struct 
    {
        uint8_t xl_g                                :1;
        uint8_t xh_g                                :1;
        uint8_t yl_g                                :1;
        uint8_t yh_g                                :1;
        uint8_t zl_g                                :1;
        uint8_t zh_g                                :1;
        uint8_t ia_g                                :1;
        uint8_t                                     :1;
    } fields;
    
    uint8_t value;
};

union LSM9DS1_STATUS_REG 
{
    struct 
    {
        uint8_t xlda                                :1;
        uint8_t gda                                 :1;
        uint8_t tda                                 :1;
        uint8_t boot_status                         :1;
        uint8_t inact                               :1;
        uint8_t ig_g                                :1;
        uint8_t ig_xl                               :1;
        uint8_t                                     :1;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG4 lsm9ds1_ctrl_reg4_t;
union LSM9DS1_CTRL_REG4 
{
    struct 
    {
        uint8_t fourd_xl1                           :1;
        uint8_t lir_xl1                             :1;
        uint8_t                                     :1;
        uint8_t xen_g                               :1;
        uint8_t yen_g                               :1;
        uint8_t zen_g                               :1;
        uint8_t                                     :2;
    } fields;
    
    uint8_t value;
};
typedef union LSM9DS1_CTRL_REG5_XL lsm9ds1_ctrl_reg5_xl_t;
union LSM9DS1_CTRL_REG5_XL
{
    struct 
    {
        uint8_t                                     :3;
        uint8_t xen_xl                              :1;
        uint8_t yen_xl                              :1;
        uint8_t zen_xl                              :1;
        uint8_t dec                                 :2;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG6_XL lsm9ds1_ctrl_reg6_xl_t;
union LSM9DS1_CTRL_REG6_XL
{
    struct 
    {
        uint8_t bw_xl                               :2;
        uint8_t bw_scal_odr                         :1;
        uint8_t fs_xl                               :2;
        uint8_t odr_xl                              :3;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG7_XL lsm9ds1_ctrl_reg7_xl_t;
union LSM9DS1_CTRL_REG7_XL
{
    struct 
    {
        uint8_t hpis1                               :1;
        uint8_t                                     :1;
        uint8_t fds                                 :1;
        uint8_t                                     :2;
        uint8_t dcf                                 :2;
        uint8_t hr                                  :1;
    } fields;
    
    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG8 lsm9ds1_ctrl_reg8_t;
union LSM9DS1_CTRL_REG8
{
    struct 
    {
        uint8_t sw_reset                            :1;
        uint8_t ble                                 :1;
        uint8_t if_add_inc                          :1;
        uint8_t sim                                 :1;
        uint8_t pp_od                               :1;
        uint8_t h_lactive                           :1;
        uint8_t bdu                                 :1;
        uint8_t boot                                :1;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG9 lsm9ds1_ctrl_reg9_t;
union LSM9DS1_CTRL_REG9
{
    struct 
    {
        uint8_t stop_on_fth                         :1;
        uint8_t fifo_en                             :1;
        uint8_t i2c_disable                         :1;
        uint8_t drdy_mask_bit                       :1;
        uint8_t fifo_temp_en                        :1;
        uint8_t                                     :1;
        uint8_t sleep_g                             :1;
        uint8_t                                     :1;
    } fields;

    uint8_t value;
};

typedef union LSM9DS1_CTRL_REG10 lsm9ds1_ctrl_reg10_t;
union LSM9DS1_CTRL_REG10 
{
    struct 
    {
        uint8_t st_xl                               :1;
        uint8_t                                     :1;
        uint8_t st_g                                :1;
        uint8_t                                     :5;
    } fields;

    uint8_t value;
};

union LSM9DS1_INT_GEN_SRC_XL
{
    struct 
    {
        uint8_t xl_xl                               :1;
        uint8_t xh_xl                               :1;
        uint8_t yl_xl                               :1;
        uint8_t yh_xl                               :1;
        uint8_t zl_xl                               :1;
        uint8_t zh_xl                               :1;
        uint8_t ia_xl                               :1;
        uint8_t                                     :1;
    } fields;

    uint8_t value;
};

union LSM9DS1_FIFO_CTRL {
    struct {
        uint8_t fth                                 :5;
        uint8_t fmode                               :3;
    } fields;
    
    uint8_t value;
};
 
union LSM9DS1_FIFO_SRC 
{
    struct 
    {
        uint8_t fss                                 :6;
        uint8_t ovrn                                :1;
        uint8_t fth                                 :1;
    } fields;

    uint8_t value;
};
 
union LSM9DS1_INT_GEN_CFG_G
{
    struct 
    {
        uint8_t xlie_g                              :1;
        uint8_t xhie_g                              :1;
        uint8_t ylie_g                              :1;
        uint8_t yhie_g                              :1;
        uint8_t zlie_g                              :1;
        uint8_t zhie_g                              :1;
        uint8_t lir_g                               :1;
        uint8_t aoi_g                               :1;
    } fields;

    uint8_t value;
};


union LSM9DS1_INT_GEN_DUR_G
{
    struct 
    {
        uint8_t wait_g                              :1;
        uint8_t dur_g                               :7;
    } fields;
    
    uint8_t value;
};

// ENUMS
enum LSM9DS1_CTRL_REG1_G_ODR
{
    LSM9DS1_CTRL_REG1_G_ODR_Power_Down              = 0x0,
    LSM9DS1_CTRL_REG1_G_ODR_14_9                    = 0x1,
    LSM9DS1_CTRL_REG1_G_ODR_59_5                    = 0x2,
    LSM9DS1_CTRL_REG1_G_ODR_119                     = 0x3,
    LSM9DS1_CTRL_REG1_G_ODR_238                     = 0x4,
    LSM9DS1_CTRL_REG1_G_ODR_476                     = 0x5,
    LSM9DS1_CTRL_REG1_G_ODR_952                     = 0x6,
    LSM9DS1_CTRL_REG1_G_ODR_na                      = 0x7
};

enum LSM9DS1_CTRL_REG6_XL_ODR
{
    LSM9DS1_CTRL_REG6_XL_ODR_Power_Down             = 0x0,
    LSM9DS1_CTRL_REG6_XL_ODR_10_Hz                  = 0x1,
    LSM9DS1_CTRL_REG6_XL_ODR_50_Hz                  = 0x2,
    LSM9DS1_CTRL_REG6_XL_ODR_119_Hz                 = 0x3,
    LSM9DS1_CTRL_REG6_XL_ODR_238_Hz                 = 0x4,
    LSM9DS1_CTRL_REG6_XL_ODR_476_Hz                 = 0x5,
    LSM9DS1_CTRL_REG6_XL_ODR_952_Hz                 = 0x6,
    LSM9DS1_CTRL_REG6_XL_ODR_NA                     = 0x7
};

enum LSM9DS1_CTRL_REG7_XL_HR_DCF
{
    LSM9DS1_CTRL_REG7_XL_HR_DCF_50                  = 0x4,
    LSM9DS1_CTRL_REG7_XL_HR_DCF_100                 = 0x5,
    LSM9DS1_CTRL_REG7_XL_HR_DCF_9                   = 0x6,
    LSM9DS1_CTRL_REG7_XL_HR_DCF_400                 = 0x7
};

enum LSM9DS1_FIFO_CTRL_FMODE
{
    LSM9DS1_FIFO_CTRL_FMODE_BYPASS                  = 0x0,
    LSM9DS1_FIFO_CTRL_FMODE_FIFO                    = 0x1,
    LSM9DS1_FIFO_CTRL_FMODE_CONTINOUS_THEN_FIFO     = 0x3,
    LSM9DS1_FIFO_CTRL_FMODE_BYPASS_THEN_CONTINOUS   = 0x4,
    LSM9DS1_FIFO_CTRL_FMODE_CONTINOUS               = 0x6
};

typedef struct LSM9DS1_OUTPUT_DATA lsm9ds1_output_data_t;
struct LSM9DS1_OUTPUT_DATA
{
    uint8_t device_id;
    union LSM9DS1_OUTPUT accel_x;
    union LSM9DS1_OUTPUT accel_y;
    union LSM9DS1_OUTPUT accel_z;
    union LSM9DS1_OUTPUT gyro_x;
    union LSM9DS1_OUTPUT gyro_y;
    union LSM9DS1_OUTPUT gyro_z;
};

/////////////////////////////////////////////////////////////////////
// Device Structure
/////////////////////////////////////////////////////////////////////
typedef struct LSM9DS1_ACCEL_GRYO_DEVICE lsm9ds1_device_t;
struct LSM9DS1_ACCEL_GRYO_DEVICE
{
    uint8_t                                         device_id;
    nrf_drv_spi_t*                                  spi;
    NRF_GPIO_Type*                                  cs_port;
    uint8_t                                         cs_pin;
    uint8_t                                         int_pin_map; // port and pin
    bool                                            enabled;
};

#endif // !(LSM9DS1_PORTMAP_H)
