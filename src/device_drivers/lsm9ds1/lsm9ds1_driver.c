#include <stdbool.h>
#include <stdint.h>

#include "lsm9ds1_driver.h"
#include "sdk_config.h"
#include "nrf52840.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "SEGGER_RTT.h"
#include "nrf_log.h"
#include "app_usbd_cdc_acm.h"

#define LSM9DS1_TRANSFER_READ   (false)
#define LSM9DS1_TRANSFER_WRITE  (true)

typedef struct LSM9DS1_REG_TRANSFER_DESC lsm9ds1_reg_transfer_desc_t;
struct LSM9DS1_REG_TRANSFER_DESC {
    bool    read_or_write;  /* read: false write: true         */
    uint8_t addr;           /* addr of register in lsm9ds1     */
    uint8_t *register_data; /* addr to store/grab data from/to */
};

static nrf_drv_spi_t left_spi = NRF_DRV_SPI_INSTANCE(0);
static nrf_drv_spi_t right_spi = NRF_DRV_SPI_INSTANCE(1);

// Configure each node, specifying
//  - spi peripheral
//  - chip select gpio port
//  - chip select gpio pin number
//  - interrupt gpio pin (port and pin combination)
//  - device id
lsm9ds1_device_t nodes[8] = {
    { /* NODE 0 */
        .spi = &left_spi,
        .cs_port = NRF_P1,
        .cs_pin = 1,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 5),
        .device_id = 0,
        .enabled = true
    },
    { /* NODE 1 */
        .spi = &left_spi,
        .cs_port = NRF_P1,
        .cs_pin = 2,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 6),
        .device_id = 1,
        .enabled = true
    },
    { /* NODE 2 */
        .spi = &left_spi,
        .cs_port = NRF_P1,
        .cs_pin = 3,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 7),
        .device_id = 2,
        .enabled = true
    },
    { /* NODE 3 */
        .spi = &left_spi,
        .cs_port = NRF_P1,
        .cs_pin = 4,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 8),
        .device_id = 3,
        .enabled = false
    },
    { /* NODE 4 */
        .spi = &right_spi,
        .cs_port = NRF_P0,
        .cs_pin = 3,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 12),
        .device_id = 4,
        .enabled = true
    },
    { /* NODE 5 */
        .spi = &right_spi,
        .cs_port = NRF_P0,
        .cs_pin = 4,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 13),
        .device_id = 5,
        .enabled = true
    },
    { /* NODE 6 */
        .spi = &right_spi,
        .cs_port = NRF_P1,
        .cs_pin = 10,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 14),
        .device_id = 6,
        .enabled = true
    },
    { /* NODE 7 */
        .spi = &right_spi,
        .cs_port = NRF_P1,
        .cs_pin = 11,
        .int_pin_map = NRF_GPIO_PIN_MAP(1, 15),
        .device_id = 7,
        .enabled = false
    }
};

bool lsm9ds1_register_transfer(lsm9ds1_reg_transfer_desc_t* transfer_desc, lsm9ds1_device_t *device);
uint32_t lsm9ds1_multiple_register_transfer(lsm9ds1_device_t *device, lsm9ds1_reg_transfer_desc_t* transfer_desc, uint32_t size);
void lsm9ds1_init(lsm9ds1_device_t *device);


void init_accels()
{
    // Config for left bus spi
    nrf_drv_spi_config_t left_spi_config = {                 
        .sck_pin      = NRF_GPIO_PIN_MAP(0, 26),                
        .mosi_pin     = NRF_GPIO_PIN_MAP(0, 27),                
        .miso_pin     = NRF_GPIO_PIN_MAP(0, 28),                
        .ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,                
        .irq_priority = SPI_DEFAULT_CONFIG_IRQ_PRIORITY,         
        .orc          = 0xFF,                                    
        .frequency    = NRF_DRV_SPI_FREQ_1M,                     
        .mode         = NRF_DRV_SPI_MODE_3,                      
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,         
    };

    // Config for right bus spi
    nrf_drv_spi_config_t right_spi_config = {                 
        .sck_pin      = NRF_GPIO_PIN_MAP(0, 29),                
        .mosi_pin     = NRF_GPIO_PIN_MAP(0, 30),                
        .miso_pin     = NRF_GPIO_PIN_MAP(0, 31),                
        .ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,                
        .irq_priority = SPI_DEFAULT_CONFIG_IRQ_PRIORITY,         
        .orc          = 0xFF,                                    
        .frequency    = NRF_DRV_SPI_FREQ_1M,                     
        .mode         = NRF_DRV_SPI_MODE_3,                      
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,         
    };

    // Initialize both busses spi peripherals
    nrf_drv_spi_init(&left_spi, &left_spi_config, NULL, NULL);
    nrf_drv_spi_init(&right_spi, &right_spi_config, NULL, NULL);

    // Initialize each lsm9ds1 (node)
    for (uint8_t i = 0; i < sizeof(nodes)/sizeof(nodes[0]); i++)
    {
        lsm9ds1_init(&nodes[i]);
    }
}

void lsm9ds1_init(lsm9ds1_device_t *device)
{
    // TODO: Finalize config, this is good for the most part though 
    lsm9ds1_ctrl_reg1_g_t   ctrl_reg1 = {
        .fields.odr_g       = LSM9DS1_CTRL_REG1_G_ODR_119, // 
        .fields.fs_g        = 1, // 500dps
        .fields.bw_g        = 0
    };

    lsm9ds1_ctrl_reg6_xl_t  ctrl_reg6 = {
        .fields.odr_xl      = LSM9DS1_CTRL_REG6_XL_ODR_119_Hz,
        .fields.fs_xl       = 0
    };

    lsm9ds1_ctrl_reg7_xl_t  ctrl_reg7 = {
        .fields.hr          = true
    };

    lsm9ds1_ctrl_reg8_t     ctrl_reg8 = {
        .fields.bdu         = true,
        .fields.if_add_inc  = true
    };

    // Setup transfer description of control registers
    lsm9ds1_reg_transfer_desc_t registers_to_write[] = {
        {
            .read_or_write  = LSM9DS1_TRANSFER_WRITE,
            .addr           = LSM9DS1_CTRL_REG1_G_ADDR,
            .register_data  = &(ctrl_reg1.value)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_WRITE,
            .addr           = LSM9DS1_CTRL_REG6_XL_ADDR,
            .register_data  = &(ctrl_reg6.value)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_WRITE,
            .addr           = LSM9DS1_CTRL_REG7_XL_ADDR,
            .register_data  = &(ctrl_reg7.value)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_WRITE,
            .addr           = LSM9DS1_CTRL_REG8_ADDR,
            .register_data  = &(ctrl_reg8.value)
        }
    };

    // Write configuration registers
    lsm9ds1_multiple_register_transfer(device, registers_to_write, sizeof(registers_to_write)/sizeof(registers_to_write[0]));
}

bool lsm9ds1_register_transfer(lsm9ds1_reg_transfer_desc_t* transfer_desc, lsm9ds1_device_t *device)
{
    ret_code_t err;
    uint8_t payload[2];

    if (transfer_desc->read_or_write == LSM9DS1_TRANSFER_READ)
    {
        // Payload structure:
        //    |      Byte0     |          Byte1          |
        //    | Addr Byte (TX) | Read Register Data (RX) |
        //
        // Its important to know that since we are using the same buffer for TX and RX, the Addr Byte will be overwritten
        // by whatever is "received" during that time - This is fine in this application
        // 
        // We need to be sure to zero-out all RX portions of the buffer to ensure we aren't reading whatever
        // was previously stored in that memory address.
        payload[0] = LSM9DS1_READ_ADDRESS(transfer_desc->addr);
        payload[1] = 0;
    }
    else // WRITE
    {
        // Payload structure, | Addr Byte (TX) | Register Data to Write (TX) |
        payload[0] = LSM9DS1_WRITE_ADDRESS(transfer_desc->addr);
        payload[1] = *(transfer_desc->register_data);
    }

    // Drive CS low as we are about to start the transfer
    device->cs_port->OUTCLR = (1 << device->cs_pin);

    // Begin transfer
    if (transfer_desc->read_or_write == LSM9DS1_TRANSFER_READ)
    {
        err = nrf_drv_spi_transfer(device->spi, payload, sizeof(payload[0]), payload, sizeof(payload)/sizeof(payload[0]));
    }
    else // WRITE
    {
        err = nrf_drv_spi_transfer(device->spi, payload, sizeof(payload)/sizeof(payload[0]), NULL, 0);
    }
    
    // Return CS high as transfer is complete
    device->cs_port->OUTSET = (1 << device->cs_pin);


    if (err != NRF_SUCCESS) {
        SEGGER_RTT_printf(0, "LSM9DS1 Register Transfer Failed\n   Error Code 0x%x Register 0x%x\n", err, transfer_desc->addr);
        
        return false;
    }

    // if read, place rx payload into register data addr
    if (transfer_desc->read_or_write == LSM9DS1_TRANSFER_READ)
    {
        *(transfer_desc->register_data) = payload[1];
    }

    return true;
}

uint32_t lsm9ds1_multiple_register_transfer(lsm9ds1_device_t *device, lsm9ds1_reg_transfer_desc_t* transfer_desc, uint32_t size)
{
    ret_code_t err;
    uint32_t successful_reads = 0;

    for (uint32_t i = 0; i < size; i++)
    {
        err = lsm9ds1_register_transfer(transfer_desc, device);

        if (err == NRF_SUCCESS)
            successful_reads++;
        
        transfer_desc++;
    }

    return successful_reads;
}


bool lsm9ds1_sample(lsm9ds1_device_t *device, lsm9ds1_output_data_t *data)
{
    // Setup transfer, essentially reading from all accel and gyro output registers
    // Storing read values in the appropriate bytes of the lsm9ds1_output_data_t struct
    lsm9ds1_reg_transfer_desc_t registers_to_read[] = {
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_X_L_XL_ADDR,
            .register_data  = &(data->accel_x.bytes.low)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_X_H_XL_ADDR,
            .register_data  = &(data->accel_x.bytes.high)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Y_L_XL_ADDR,
            .register_data  = &(data->accel_y.bytes.low)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Y_H_XL_ADDR,
            .register_data  = &(data->accel_y.bytes.high)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Z_L_XL_ADDR,
            .register_data  = &(data->accel_z.bytes.low)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Z_H_XL_ADDR,
            .register_data  = &(data->accel_z.bytes.high)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_X_L_G_ADDR,
            .register_data  = &(data->gyro_x.bytes.low)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_X_H_G_ADDR,
            .register_data  = &(data->gyro_x.bytes.high)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Y_L_G_ADDR,
            .register_data  = &(data->gyro_y.bytes.low)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Y_H_G_ADDR,
            .register_data  = &(data->gyro_y.bytes.high)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Z_L_G_ADDR,
            .register_data  = &(data->gyro_z.bytes.low)
        },
        {
            .read_or_write  = LSM9DS1_TRANSFER_READ,
            .addr           = LSM9DS1_OUT_Z_H_G_ADDR,
            .register_data  = &(data->gyro_z.bytes.high)
        }
    };

    // Set device id of data structure to provided devices id
    data->device_id = device->device_id;

    // Begin transfer
    lsm9ds1_multiple_register_transfer(device, registers_to_read, sizeof(registers_to_read)/sizeof(registers_to_read[0]));

    // TODO: error checking
    return true;
}

void lsm9ds1_avg_samples(lsm9ds1_output_data_t *sample1, lsm9ds1_output_data_t *sample2)
{
    sample1->accel_x.value  += sample2->accel_x.value;
    sample1->accel_y.value  += sample2->accel_y.value;
    sample1->accel_z.value  += sample2->accel_z.value;
    sample1->gyro_x.value   += sample2->gyro_x.value;
    sample1->gyro_y.value   += sample2->gyro_y.value;
    sample1->gyro_z.value   += sample2->gyro_z.value;
    sample1->accel_x.value  /= 2;
    sample1->accel_y.value  /= 2;
    sample1->accel_z.value  /= 2;
    sample1->gyro_x.value   /= 2;
    sample1->gyro_y.value   /= 2;
    sample1->gyro_z.value   /= 2;
}