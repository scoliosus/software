/**CFile***********************************************************************
  FileName    [ sampling_task.c ]
  PackageName [ tasks ]
  Synopsis    [ required ]
  Description [ optional ]
  SeeAlso     [ optional ]
  Author      [ Dalin Riches ]
******************************************************************************/
#include "sampling_task.h"
#include "lsm9ds1_driver.h"
#include "usb_transport.h"
#include "timer.h"
#include "nrf_gpio.h"
#include "SEGGER_RTT.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Stucture declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef enum SAMPLING_STATE sampling_state_t;
enum SAMPLING_STATE
{
	SAMPLING_DISABLED,
	SAMPLING_DATA,
	SAMPLING_TARGET
};

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
void sampling_process_events(EventBits_t events_in);
void sample_imus (void);

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
TaskHandle_t 		sampling_task_handle;
EventGroupHandle_t 	sampling_events_in_handle;

sampling_state_t	sampling_state = SAMPLING_DISABLED;
uint32_t			sample_count_since_last_packet;

lsm9ds1_output_data_t output_data[8]; /** Store current output data **/
lsm9ds1_output_data_t sample_buff; /** Store current sample **/
/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/ 

/*---------------------------------------------------------------------------*/
/* Definition of external functions                                          */
/*---------------------------------------------------------------------------*/
void sampling_task_function()
{
	EventBits_t events_in;

	init_timer(NRF_TIMER2);
	timer_set_trigger_freq(NRF_TIMER2, 50);
	timer_start(NRF_TIMER2);

	while(true)
	{
		events_in = xEventGroupWaitBits(
						sampling_events_in_handle,
						(SAMPLING_INEVENT_SAMPLE_TICK_MSK|SAMPLING_INEVENT_TARGET_MODE_MSK|SAMPLING_INEVENT_ENABLE_MSK),
						pdTRUE,
						pdFALSE,
						portMAX_DELAY
		);
		
		sampling_process_events(events_in);
	}
}


/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/
void sampling_process_events(EventBits_t events_in)
{
	// Make required state transitions
	switch (sampling_state)
	{
		case SAMPLING_DISABLED:
			if ((events_in & SAMPLING_INEVENT_ENABLE_MSK) == SAMPLING_INEVENT_ENABLE_MSK)
			{
				sampling_state = SAMPLING_TARGET;
				sample_count_since_last_packet = 0;
				SEGGER_RTT_printf(0, "Sampling: Enabled\n");

				nrf_gpio_pin_clear(NRF_GPIO_PIN_MAP(0,13));
				nrf_gpio_pin_clear(NRF_GPIO_PIN_MAP(0,14));
				usb_packet_send_start();
			}
			break;

		case SAMPLING_DATA:
			if ((events_in & SAMPLING_INEVENT_ENABLE_MSK) == SAMPLING_INEVENT_ENABLE_MSK)
			{
				sampling_state = SAMPLING_DISABLED;
				sample_count_since_last_packet = 0;
				nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,13));
				nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,14));
				usb_packet_send_stop();
			}
			else if ((events_in & SAMPLING_INEVENT_TARGET_MODE_MSK) == SAMPLING_INEVENT_TARGET_MODE_MSK)
			{
				sampling_state = SAMPLING_TARGET;
				sample_count_since_last_packet = 0;
				nrf_gpio_pin_clear(NRF_GPIO_PIN_MAP(0,14));
			}
			break;
		case SAMPLING_TARGET:
			if ((events_in & SAMPLING_INEVENT_ENABLE_MSK) == SAMPLING_INEVENT_ENABLE_MSK)
			{
				sampling_state = SAMPLING_DISABLED;
				sample_count_since_last_packet = 0;
				nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,13));
				nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,14));
				usb_packet_send_stop();
			}
			break;

		default:
			break;
	}

	// Now that state transitions have been handled
	// If a sample tick has been received handle it (based on state)
	if ((events_in & SAMPLING_INEVENT_SAMPLE_TICK_MSK) == SAMPLING_INEVENT_SAMPLE_TICK_MSK)
	{
		switch (sampling_state)
		{
			case SAMPLING_DATA:
				sample_imus();

				if (sample_count_since_last_packet >= SAMPLES_PER_PACKET_DATA)
				{
					sample_count_since_last_packet = 0;

					SEGGER_RTT_printf(0, "Sampled: DATA\n");

					usb_packet_send_sample(output_data, sizeof(nodes)/sizeof(nodes[0]));
				}
				break;

			case SAMPLING_TARGET:
				sample_imus();

				if (sample_count_since_last_packet >= SAMPLES_PER_PACKET_TARGET)
				{
					sampling_state = SAMPLING_DATA;
					sample_count_since_last_packet = 0;

					SEGGER_RTT_printf(0, "Sampled: TARGET\n");

					nrf_gpio_pin_set(NRF_GPIO_PIN_MAP(0,14));
					usb_packet_send_target(output_data, sizeof(nodes)/sizeof(nodes[0]));
				}
				break;
			case SAMPLING_DISABLED:
				break;
			default:
				break;
		}
	}
}

void sample_imus (void)
{
	for (uint8_t i = 0; i < sizeof(nodes)/sizeof(nodes[0]); i++)
	{
		if (!nodes[i].enabled)
			continue;

		if (sample_count_since_last_packet == 0)
		{
			lsm9ds1_sample(&nodes[i], &output_data[i]);
			continue;
		}

		lsm9ds1_sample(&nodes[i], &sample_buff);
		lsm9ds1_avg_samples(&output_data[i], &sample_buff);

		SEGGER_RTT_printf(0, "DEVICE: %d x: %d y: %d z: %d\n", output_data[i].device_id, output_data[i].accel_x.value, output_data[i].accel_y.value, output_data[i].accel_z.value);
	}
	
	sample_count_since_last_packet++;
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/
