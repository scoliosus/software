#ifndef SAMPLING_TASK_H
#define SAMPLING_TASK_H

/*---------------------------------------------------------------------------*/
/* Includes                                                                  */
/*---------------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/
#define SAMPLING_INEVENT_SAMPLE_TICK_MSK		 	(1 << 0)
#define SAMPLING_INEVENT_TARGET_MODE_MSK			(1 << 3)
#define SAMPLING_INEVENT_ENABLE_MSK					(1 << 4)

#define SAMPLES_PER_PACKET_DATA						(5)
#define SAMPLES_PER_PACKET_TARGET					(50)

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef enum SAMPLING_MODE sampling_mode_t;
enum SAMPLING_MODE 
{
	SAMPLING_MODE_DATA,
	SAMPLING_MODE_TARGET
};

/*---------------------------------------------------------------------------*/
/* Stucture declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
extern TaskHandle_t 		sampling_task_handle;
extern EventGroupHandle_t   sampling_events_in_handle;
/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
void sampling_task_function();

#endif // !SAMPLING_TASK_H