#include "flash_memory.h"
#include "fds.h"
#include "SEGGER_RTT.h"
#include "imu_data_processing.h"

#include <stdint.h>
#include <string.h>
#include "nrf.h"

//static char const m_hello[]  = "Hello, world!";
static double *tilt_angles[NUM_AXIS];
//static double tilt_angles;


static fds_record_t m_dummy_record =
{
    .file_id           = CONFIG_FILE, 
    .key               = CONFIG_REC_KEY,
    .data.p_data       = &tilt_angles,
    /* The length of a record is always expressed in 4-byte units (words). */
    .data.length_words = (sizeof(tilt_angles) + 3),
};

fds_record_desc_t desc;

static void fds_evt_handler(fds_evt_t const * p_evt)
{

    //SEGGER_RTT_printf(0, "EVENT: %d\n", p_evt->id);
    switch (p_evt->id)
        {
            case FDS_EVT_INIT:
                if (p_evt->result == FDS_SUCCESS)
                {
                    SEGGER_RTT_printf(0, "Init Success\n");
                }
                else 
                {
                    SEGGER_RTT_printf(0, "Init Fail\n");
                }
                break;

            case FDS_EVT_WRITE:
            {
                if (p_evt->result == FDS_SUCCESS)
                {
                    SEGGER_RTT_printf(0, "Write Success\n");
                } 
                else 
                {
                    SEGGER_RTT_printf(0, "Write Fail\n");
                }
            } break;

            case FDS_EVT_DEL_RECORD:
            {
                if (p_evt->result == FDS_SUCCESS)
                {
                    //SEGGER_RTT_printf(0, "Delete Success\n");
                }
                else 
                {
                    SEGGER_RTT_printf(0, "Delete Fail\n");
                }
            } break;

            default:
                break; 
        }
}

void memory_test(double *rotation_angles){
    SEGGER_RTT_printf(0, "START OF NEW TEST\n");
    //rotation_angles[0] = 5;
    print_rotation_angles(rotation_angles);
    
    m_dummy_record.data.p_data = rotation_angles;
    
    //SEGGER_RTT_printf(0, "DATA TEST: %d\n", &rotation_angles[1]);
    print_rotation_angles((double *) m_dummy_record.data.p_data);
    //tilt_angles[NUM_AXIS] = rotation_angles[NUM_AXIS];

    ret_code_t rc = fds_register(fds_evt_handler);
    rc = fds_init();

    
    rc = fds_record_write(&desc, &m_dummy_record);
    if (rc != FDS_SUCCESS){
        SEGGER_RTT_printf(0, "Write Fail");
    }

    fds_flash_record_t config;
    //fds_record_t record_test = {0};
    rc = fds_record_open(&desc, &config);
    /* Copy the configuration from flash into m_dummy_cfg. */
    //SEGGER_RTT_printf(0, "FILE_ID:%d\n", config.file_id);

    print_rotation_angles((double *) config.p_data);

    if (fds_record_close(&desc) != FDS_SUCCESS){
        SEGGER_RTT_printf(0, "Close Fail");
    }
    else {
        SEGGER_RTT_printf(0, "Close Success");
    }

    rc = fds_record_delete(&desc);
    if (rc == FDS_SUCCESS){
        SEGGER_RTT_printf(0, "DELETE SUCCESS");
    }

    //SEGGER_RTT_printf(0, "DATA: %s\n", config.p_data);
    //SEGGER_RTT_printf(0, "DATA: %s\n", config.p_data);

}

