#include "imu_data_processing.h"

void print_rotation_angles(double * values) {

    for (int i = 0; i < NUM_AXIS; i++) {
        char str[100];

        char *tmpSign = (values[i] < 0) ? "-" : "";
        float tmpVal = (values[i] < 0) ? -values[i] : values[i];

        int tmpInt1 = tmpVal;                  // Get the integer
        float tmpFrac = tmpVal - tmpInt1;      // Get fraction
        int tmpInt2 = trunc(tmpFrac * 10000);  // Turn into integer

        sprintf (str, "Angle %d = %s%d.%04d\n", i, tmpSign, tmpInt1, tmpInt2);

        SEGGER_RTT_WriteString(0, str);
    }
}

void sample_accel(struct LSM9DS1_ACCEL_GRYO_DEVICE *device, double * tilt_angles, int16_t * a) {

    a[X_AXIS] = lsm9ds1_read_accel_x(device);
    a[Y_AXIS] = lsm9ds1_read_accel_y(device);
    a[Z_AXIS] = lsm9ds1_read_accel_z(device);

    calculate_rotation_angles(tilt_angles, a);
}

void calculate_rotation_angles(double * tilt_angles, int16_t * a) {

    double accel_g[NUM_AXIS] = { 0.0 };

    accel_g[X_AXIS] = a[X_AXIS]*SCALE_FACTOR;
    accel_g[Y_AXIS] = a[Y_AXIS]*SCALE_FACTOR;
    accel_g[Z_AXIS] = a[Z_AXIS]*SCALE_FACTOR;

    tilt_angles[X_AXIS] = atan(accel_g[Z_AXIS]/accel_g[Y_AXIS])*RAD_TO_DEG_SCALE_FACTOR;
    tilt_angles[Y_AXIS] = atan(accel_g[X_AXIS]/accel_g[Z_AXIS])*RAD_TO_DEG_SCALE_FACTOR;
    tilt_angles[Z_AXIS] = atan(accel_g[Y_AXIS]/accel_g[X_AXIS])*RAD_TO_DEG_SCALE_FACTOR;
    //tilt_angles[X_AXIS] = 5;
    //tilt_angles[Y_AXIS] = 6;
    //tilt_angles[Z_AXIS] = 5;

    //print_rotation_angles(tilt_angles);
}