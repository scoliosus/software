#ifndef ACCEL_FUN_H
#define ACCEL_FUN_H

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "SEGGER_RTT.h"
#include "lsm9ds1_driver.h"

#define NUM_AXIS 3
#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2
#define PI 3.14159265
#define SCALE_FACTOR 2.0/32768.0
#define RAD_TO_DEG_SCALE_FACTOR 180/PI

void print_rotation_angles(double * values);
void sample_accel(struct LSM9DS1_ACCEL_GRYO_DEVICE *device, double * tilt_angles, int16_t * a);
void calculate_rotation_angles(double * tilt_angles, int16_t * a);

#endif // !ACCEL_FUN_H